#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import urllib2
from decimal import *
getcontext().prec=5

m = file('municipios.html','r')
import os
d3 = file('gastoesporte2012.txt','w')
d2 = file('gastoesporte2010.txt','a')
d1 = file('gastoesporte2007.txt','a')
l = ''

municipios = ['31*00000620*0*1">BELO HORIZONTE</option>','31*00000670*5*1">BETIM</option>']


def extrai2010(municipio, ano=2010):


    #municipio = '31*00000180*5*1">ALPERCATA</option>'
    url = 'https://www.contaspublicas.caixa.gov.br/sistncon_internet/consultaDeclaracoes.do'
    #url ='http://localhost:8080'
    url2 = 'https://www.contaspublicas.caixa.gov.br/sistncon_internet/consultaDeclaracoes.do?acao=imprimir&numeroDeclaracao='
    opener = urllib2.build_opener(urllib2.HTTPHandler(debuglevel=1))
    form = dict(name='consultaForm',acao='pesquisar',pagina='1',numeroDeclaracao='0',esfera='1',codEstado='31',
                codMunicipio=municipio[:15],codTipoPoder='1',
                codTipoOrgao='6',anoBase=ano, codigosPeriodo='01*003',numeroItensPagina=10)
    data = urllib.urlencode(form)
    content = opener.open(url, data=data).read()
    lines = content.split('\r')
    desporto =''
    total = ''
    for line in lines:
        a = line.find('numDeclaracao')
        if a != -1:
            #print(line)
            v = line.split('value="')
            v1 = v[1].split('*')
            #print(v1[0])
            pdf = opener.open(url2+v1[0])

            from PyPDF2 import PdfFileReader
            data = pdf.read()
            pdf.close()
            opener.close()

            FILE = open('temp.pdf', "wb")
            FILE.write(data)
            FILE.close()


            p = os.system('pdftotext -layout temp.pdf')
            f = file('temp.txt','rb')

            fesp = False
            flegislativa = False
            ftotal = False

            for line in f.readlines():
                t = line.find('Despesa Total')
                if ftotal == False and t != -1:
                    if ano < 2009:
                        total = line[88:114].strip()
                    else:
                        total = line[115:134].strip()
                    print(total)
                    ftotal = True
                a = line.find('Desporto e Lazer =')
                if fesp == False and a != -1:
                    #print(line)
                    desporto = line[70:88].strip()
                    fesp = True
                a = line.find('Legislativa =')
                if fesp == False and a != -1:
                    #print(line)
                    desporto = line[70:88].strip()
                    fesp = True
    if (desporto == '' and total == '') or (desporto=='0,00' and total=='0,00'):
        print('deu biziu')
        return extrai2010(municipio,ano-1)

    codmun = municipio[:2]+municipio[7:11]+municipio[12:13]

    r = "'%s'; '%s'; %s;%s;%s; \r"  % (codmun,municipio.split(">")[1].split('<')[0],
                                        desporto,total,ano)
    print(r)
    f.close()
    return r


def extrai2012(municipio, ano=2012):


    #municipio = '31*00000180*5*1">ALPERCATA</option>'
    url = 'https://www.contaspublicas.caixa.gov.br/sistncon_internet/consultaDeclaracoes.do'
    #url ='http://localhost:8080'
    url2 = 'https://www.contaspublicas.caixa.gov.br/sistncon_internet/consultaDeclaracoes.do?acao=imprimir&numeroDeclaracao='
    opener = urllib2.build_opener(urllib2.HTTPHandler(debuglevel=1))
    form = dict(name='consultaForm',acao='pesquisar',pagina='1',numeroDeclaracao='0',esfera='1',codEstado='31',
                codMunicipio=municipio[:15],codTipoPoder='1',
                codTipoOrgao='6',anoBase=ano, codigosPeriodo='01*003',numeroItensPagina=10)
    data = urllib.urlencode(form)
    content = opener.open(url, data=data).read()
    lines = content.split('\r')
    desporto =''
    total = ''
    for line in lines:
        a = line.find('numDeclaracao')
        if a != -1:
            #print(line)
            v = line.split('value="')
            v1 = v[1].split('*')
            #print(v1[0])
            pdf = opener.open(url2+v1[0])

            from PyPDF2 import PdfFileReader
            data = pdf.read()
            pdf.close()
            opener.close()

            FILE = open('temp.pdf', "wb")
            FILE.write(data)
            FILE.close()


            p = os.system('pdftotext -layout temp.pdf')
            f = file('temp.txt','rb')

            found = False
            foundt = False
            fleg = False
            fjud = False
            fsaude = False
            feduc = False
            for line in f.readlines():
                t = line.find('Despesa Total')
                if foundt == False and t != -1:
                    if line[110:112] == '74':
                        total = line[113:132].strip()
                    elif line[110:111] != ' ':
                        total = line[113:130].strip()
                    else:
                        total = line[112:125].strip()
                    if total == '0,00':
                        total = line[92:110].strip()
                    foundt = True
                a = line.find('Legislativa =')
                if fleg == False and a != -1:
                    #print(line)                    if desporto == '0,00':
                    if municipio in ['BELO HORIZONTE','BETIM']:
                        legislativa = line[109:127 ].strip()
                    else:
                        legislativa = line[108:124 ].strip()
                    fleg = True
                a = line.find('Saúde =')
                if fsaude == False and a != -1:
                    if municipio in ['BELO HORIZONTE','BETIM']:
                        saude = line[110:129].strip()
                    else:
                        saude = line[110:127].strip()

                    fsaude = True
                a = line.find('Educação =')
                if feduc == False and a != -1:
                    #print(line)                    if desporto == '0,00':
                    if municipio in ['BELO HORIZONTE','BETIM']:
                        educacao = line[104:12].strip()
                    else:
                        educacao = line[98:114].strip()
                    feduc = True
                a = line.find('Desporto e Lazer =')
                if found == False and a != -1:
                    #print(line)
                    desporto = line[131:152].strip()
                    if desporto == '0,00':
                        desporto = line[108:130].strip()
                    found = True

    codmun = municipio[:2]+municipio[7:11]+municipio[12:13]
    try:
        r = "'%s'; '%s'; %s;%s;%s;%s;%s;%s; \r"  % (codmun,municipio.split(">")[1].split('<')[0],ano, total, desporto,
            legislativa,saude,educacao)

    except:
        r = "'%s'; '%s'; %s;%s;%s;%s;%s;%s; \r"  % (codmun,municipio.split(">")[1].split('<')[0],ano, '0.00', '0.00','0.00','0.00','0.00')
    print(r)

    return r
r = "'codigo_municipio';'municipio';'ano','despesatotal';'desportoelazer';'legislativa';'saude','educacao';\r"
d3.write(r)
for municipio in  m.readlines():
    #for municipio in  municipios:
    print(municipio)

    #r =  extrai2012(municipio)
    #d3.write(r)
    #r = extrai2010(municipio)
    #d2.write(r)
    r = extrai2010(municipio,2007)
    #d1.write(r)

d3.close()
d2.close()
d1.close()

