from bottle import route, run, post, request

@post('/')
def hello():
    for value in request.forms:
        print(value)
    return "Hello World!"

run(host='localhost', port=8080, debug=True)